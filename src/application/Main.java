package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	private Parent root;
	@Override
	public void start(Stage primaryStage) {
		try {
	        FXMLLoader fxmlLoader= new FXMLLoader (getClass().getResource("Layout.fxml"));
	        this.root = fxmlLoader.load();
	        primaryStage.setTitle("TCP UDP tester Client");
	        primaryStage.setScene(new Scene(root, 410, 169));
	        primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
