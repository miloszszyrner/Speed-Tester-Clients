package application;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class TCPThread extends Thread {

	int port;
	int buffsize;
	String ipAddress;
	boolean nagle;
	private Socket socket = null;
	DataOutputStream dOut = null;
	volatile boolean finished = false;

	public TCPThread(int port, int buffsize, String ipAddress, boolean nagle) {
		super();
		this.port = port;
		this.buffsize = buffsize;
		this.ipAddress = ipAddress;
		this.nagle = nagle;
		initialize();
	}

	private void initialize() {
		try {
			socket = new Socket(ipAddress, port);
			socket.setTcpNoDelay(nagle);
			dOut = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void stopMe() throws InterruptedException {
		finished = true;
		try {
			dOut.close();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String text = "SIZE:" + buffsize + "\n";
		byte[] buf = new byte[buffsize];

		try {
			dOut.writeBytes(text);
			dOut.flush();
			sleep(2);
			while (!finished) {
				dOut = new DataOutputStream(socket.getOutputStream());
				dOut.write(buf);
				dOut.flush();
				sleep(10);
			}

		} catch (IOException e) {
			try {
				finished = true;
				dOut.close();
				socket.close();
				sleep(2);
				this.stop();
				
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		} catch (InterruptedException e) {
		}
		
		
	}

	public Socket getSocket() {
		return socket;
	}
}
