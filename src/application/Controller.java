package application;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

public class Controller {
	@FXML
	public TextField addressTextField;

	@FXML
	public TextField listeningPortTextField;

	@FXML
	public Slider bufferSizeSlider;

	@FXML
	public ToggleButton nagleToggleButton;

	@FXML
	public Button startTransmissionButton;

	@FXML
	public Label TCPlabel;

	@FXML
	public Label UDPlabel;

	@FXML
	public Label dataSizeLabel;

	@FXML
	public Button stopTransmisiionButton;
	UDPThread udpThread;
	TCPThread tcpThread;

	private Pattern pattern;
	private Matcher matcher;

	private static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	@FXML
	public void initialize() {
		dataSizeLabel.setText((int) bufferSizeSlider.getValue() + " bytes");
		bufferSizeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
			dataSizeLabel.setText((int) bufferSizeSlider.getValue() + " bytes");
		});
		startTransmissionButton.setDisable(false);
		stopTransmisiionButton.setDisable(true);
		pattern = Pattern.compile(IPADDRESS_PATTERN);
	}

	@FXML
	private void handleToggleButtonAction(ActionEvent event) throws IOException {
		if (nagleToggleButton.isSelected()) {
			nagleToggleButton.setText("ON");
		} else {
			nagleToggleButton.setText("OFF");
		}
	}

	@FXML
	private void handleStartButtonAction(ActionEvent event) throws IOException {
		if (validate(addressTextField.getText()) && validateListeningPort()) {
			udpThread = new UDPThread(Integer.parseInt(listeningPortTextField.getText()),
					(int) bufferSizeSlider.getValue(), addressTextField.getText(), nagleToggleButton.isSelected());
			tcpThread = new TCPThread(Integer.parseInt(listeningPortTextField.getText()),
					(int) bufferSizeSlider.getValue(), addressTextField.getText(), nagleToggleButton.isSelected());
			udpThread.start();
			tcpThread.start();
			tcpThread.finished = false;
			startTransmissionButton.setDisable(true);
			stopTransmisiionButton.setDisable(false);
			addressTextField.setEditable(false);
			listeningPortTextField.setEditable(false);
			bufferSizeSlider.setDisable(true);
			if (udpThread.isAlive()) {
				UDPlabel.setText("UDP thread: running");
			}
			if (tcpThread.isAlive()) {
				TCPlabel.setText("TCP thread: running");
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR, "Wrong ip address or port number");
			alert.showAndWait();
		}
	}

	private boolean validateListeningPort() {
		if (listeningPortTextField.getText().length() != 0)
			try {
				return Integer.parseInt(listeningPortTextField.getText()) > 0
						&& Integer.parseInt(listeningPortTextField.getText()) < 65535;
			} catch (NumberFormatException e) {
				Alert alert = new Alert(AlertType.ERROR, "Wrong port number");
				alert.showAndWait();
			}
		return false;
	}

	@SuppressWarnings("deprecation")
	@FXML
	private void handleStopTransmisiion(ActionEvent event) throws IOException, InterruptedException {

		startTransmissionButton.setDisable(false);
		stopTransmisiionButton.setDisable(true);
		bufferSizeSlider.setDisable(false);
		addressTextField.setEditable(true);
		listeningPortTextField.setEditable(true);

		tcpThread.stopMe();
		Thread.sleep(2);
		udpThread.stopMe();

		Thread.sleep(2);

		tcpThread.stop();
		Thread.sleep(2);
		udpThread.stop();

		Thread.sleep(2);
		if (!udpThread.isAlive()) {
			UDPlabel.setText("UDP thread: stopped");
		}
		if (!tcpThread.isAlive()) {
			TCPlabel.setText("TCP thread: disconnected");
		}
	}

	public boolean validate(final String ip) {
		matcher = pattern.matcher(ip);
		return matcher.matches();
	}

}
