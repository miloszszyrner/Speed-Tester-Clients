package application;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPThread extends Thread {
	volatile boolean finished = false;
	int port;
	int buffsize;
	String ipAddress;
	boolean nagle;
	private DatagramSocket socket = null;
	InetAddress address = null;

	public UDPThread(int port, int buffsize, String ipAddress, boolean nagle) {
		this.port = port;
		this.buffsize = buffsize;
		this.ipAddress = ipAddress;
		this.nagle = nagle;
		initialize();
	}

	private void initialize() {

		try {
			socket = new DatagramSocket();
			address = InetAddress.getByName(ipAddress);
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void stopMe() {
		finished = true;
		String text = "FINE";
		byte[] buf = new byte[buffsize];

		DatagramPacket size = new DatagramPacket(text.getBytes(), text.getBytes().length, address, port);
		try {
			socket.send(size);
			sleep(2);
		} catch (IOException e1) {
			socket.close();
			this.stop();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		socket.close();
	}

	@SuppressWarnings("deprecation")
	public void run() {
		while (!finished) {
			String text = "SIZE:" + buffsize;
			byte[] buf = new byte[buffsize];

			DatagramPacket size = new DatagramPacket(text.getBytes(), text.getBytes().length, address, port);
			try {
				socket.send(size);
				sleep(2);
			} catch (IOException e1) {
				socket.close();
				this.stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
			while (!finished) {
				try {
					socket.send(packet);
					sleep(10);
				} catch (IOException e) {
					socket.close();
					try {
						sleep(2);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					this.stop();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
		}
	}

	public DatagramSocket getSocket() {
		return socket;
	}
}
